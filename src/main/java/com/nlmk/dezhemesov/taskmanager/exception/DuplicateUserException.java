package com.nlmk.dezhemesov.taskmanager.exception;

/**
 * Исключение: Дубликат пользователя
 */
public class DuplicateUserException extends Exception {
}
