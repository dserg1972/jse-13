package com.nlmk.dezhemesov.taskmanager.repository;

import java.util.LinkedList;
import java.util.List;

/**
 * Хранилище истории команд
 */
public class HistoryRepository {

    /**
     * Список команд
     */
    private final LinkedList<String> history = new LinkedList<>();

    /**
     * Создание записи истории
     *
     * @param command текст команды
     */
    public void create(final String command) {
        history.add(command);
    }

    /**
     * Удаление первой записи
     */
    public void removeFirst() {
        history.removeFirst();
    }

    /**
     * Удаление всех записей
     */
    public void clear() {
        history.clear();
    }

    /**
     * Получение списка записей
     *
     * @return Список записей
     */
    public List<String> findAll() {
        return history;
    }

    /**
     * Получение количества хранящихся записей истории
     *
     * @return количество записей
     */
    public int size() {
        return history.size();
    }

}
