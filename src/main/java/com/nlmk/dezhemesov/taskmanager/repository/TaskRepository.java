package com.nlmk.dezhemesov.taskmanager.repository;

import com.nlmk.dezhemesov.taskmanager.entity.Task;
import com.nlmk.dezhemesov.taskmanager.util.Identifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Хранилище задач
 */
public class TaskRepository {

    /**
     * Список задач
     */
    private final List<Task> tasks = new ArrayList<>();

    /**
     * Идентификатор текущего пользователя
     */
    private Identifier userId = null;

    /**
     * Конструктор
     *
     * @param userId идентификатор текущего пользователя
     */
    public TaskRepository(final Identifier userId) {
        this.userId = userId;
    }

    /**
     * Создание задачи c именем и добавление в хранилище
     *
     * @param name имя задачи
     * @return созданная задача
     */
    public Task create(final String name) {
        final Task task = new Task(name, userId.getIdentifier());
        tasks.add(task);
        return task;
    }

    /**
     * Создание задачи с именем и описанием и добавление в хранилище
     *
     * @param name        имя задачи
     * @param description описание задачи
     * @return созданная задача
     */
    public Task create(final String name, final String description) {
        final Task task = new Task(name, userId.getIdentifier(), description);
        tasks.add(task);
        return task;
    }

    /**
     * Очистка хранилища
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Список задач
     *
     * @return список задач
     */
    public List<Task> findAll() {
        return tasks.stream().filter(t -> t.getUserId().equals(userId.getIdentifier())).collect(Collectors.toList());
    }

    /**
     * Поиск задачи в хранилище по индексу
     *
     * @param index индекс задачи в хранилище
     * @return найденная задача либо null, если задача не найдена либо неправильный индекс
     */
    public Task findByIndex(final int index) {
        return findAll().get(index);
    }

    /**
     * Поиск задачи в хранилище по имени
     *
     * @param name имя задачи
     * @return найденная задача либо null, если задача не найдена
     */
    public Task findByName(final String name) {
        Optional<Task> taskOptional = findAll().stream().filter(t -> name.equals(t.getName())).findFirst();
        return taskOptional.orElse(null);
    }

    /**
     * Поиск задачи в хранилище по идентификатору
     *
     * @param id идентификатор задачи
     * @return найденная задача либо null, если задача не найдена
     */
    public Task findById(final Long id) {
        Optional<Task> taskOptional = findAll().stream().filter(t -> id.equals(t.getId())).findFirst();
        return taskOptional.orElse(null);
    }

    /**
     * Получение списка задач по идентификатору проекта
     *
     * @param projectId идентификатор проекта
     * @return список задач, соответствующих условию поиска
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        List<Task> list;
        list = findAll().stream().filter(
                task -> (projectId == null && task.getProjectId() == null) ||
                        (projectId != null && projectId.equals(task.getProjectId()))
        ).collect(Collectors.toList());
        return list;
    }

    /**
     * Проверка принадлежности задачи проекту
     *
     * @param projectId идентификатор проекта
     * @param taskId    идентификатор задачи
     * @return задача
     */
    public Task findByProjectIdAndTaskId(final Long projectId, final Long taskId) {
        if (taskId == null) return null;
        Optional<Task> optionalTask = findAll().stream().filter(task -> taskId.equals(task.getId()) &&
                projectId.equals(task.getProjectId())).findFirst();
        return optionalTask.orElse(null);
    }

    /**
     * Получение размера хранилища
     *
     * @return Размер хранилища
     */
    public int size() {
        return findAll().size();
    }

}
