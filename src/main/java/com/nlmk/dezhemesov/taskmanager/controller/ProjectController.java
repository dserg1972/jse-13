package com.nlmk.dezhemesov.taskmanager.controller;

import com.nlmk.dezhemesov.taskmanager.entity.Project;
import com.nlmk.dezhemesov.taskmanager.entity.Task;
import com.nlmk.dezhemesov.taskmanager.service.ProjectService;
import com.nlmk.dezhemesov.taskmanager.service.ProjectTaskService;

import java.util.List;

/**
 * Контроллер проектов
 */
public class ProjectController extends AbstractController {

    /**
     * Служба проектов
     */
    private final ProjectService projectService;
    /**
     * Служба связанной бизнес-логики
     */
    private final ProjectTaskService projectTaskService;

    /**
     * Конструктор
     *
     * @param projectService служба проектов
     */
    public ProjectController(final ProjectService projectService, final ProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    /**
     * Просмотр проекта по индексу
     *
     * @return код возврата
     */
    public int viewByIndex() {
        System.out.print("Enter index: ");
        final Integer index = readInteger();
        if (index == null) {
            System.out.println("Invalid input!");
            return 0;
        }
        Project project = projectService.findByIndex(index - 1);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр проекта по идентификатору
     *
     * @return код возврата
     */
    public int viewById() {
        System.out.print("Enter id: ");
        final Long id = readLong();
        if (id == null) {
            System.out.println("Invalid input!");
            return 0;
        }
        Project project = projectService.findById(id);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр проекта по имени
     *
     * @return код возврата
     */
    public int viewProjectByName() {
        System.out.print("Enter name: ");
        final String name = readString();
        Project project = projectService.findByName(name);
        viewProject(project);
        return 0;
    }

    /**
     * Редкатирование проекта по индексу
     *
     * @return код возврата
     */
    public int updateProjectByIndex() {
        System.out.print("Enter index: ");
        final Integer index = readInteger();
        if (index == null) {
            System.out.println("Invalid input!");
            return 0;
        }
        Project project = projectService.findByIndex(index - 1);
        updateProject(project);
        return 0;
    }

    /**
     * Редактирование проекта по идентификатору
     *
     * @return код возврата
     */
    public int updateProjectById() {
        System.out.print("Enter id: ");
        final Long id = readLong();
        if (id == null) {
            System.out.println("Invalid input!");
            return 0;
        }
        Project project = projectService.findById(id);
        updateProject(project);
        return 0;
    }

    /**
     * Редактирование проекта по имени
     *
     * @return код возврата
     */
    public int updateProjectByName() {
        System.out.print("Enter name: ");
        final String name = readString();
        Project project = projectService.findByName(name);
        updateProject(project);
        return 0;
    }

    /**
     * Удаление проекта по индексу
     *
     * @return код возврата
     */
    public int removeProjectByIndex() {
        System.out.print("Enter index: ");
        final Integer index = readInteger();
        if (index == null) {
            System.out.println("Invalid input!");
            return 0;
        }
        Project project = projectService.findByIndex(index - 1);
        removeProject(project);
        return 0;
    }

    /**
     * Удаление проекта по идентификатору
     *
     * @return код возврата
     */
    public int removeProjectById() {
        System.out.print("Enter id: ");
        final Long id = readLong();
        if (id == null) {
            System.out.println("Invalid input!");
            return 0;
        }
        Project project = projectService.findById(id);
        removeProject(project);
        return 0;
    }

    /**
     * Удаление проекта по имени
     *
     * @return код возврата
     */
    public int removeProjectByName() {
        System.out.print("Enter name: ");
        final String name = readString();
        Project project = projectService.findByName(name);
        removeProject(project);
        return 0;
    }

    /**
     * Просмотр атрибутов проекта
     *
     * @param project проект
     * @return проект
     */
    private Project viewProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Projects tasks:");
        List<Task> taskList = projectTaskService.findAllByProjectId(project.getId());
        taskList.forEach(task -> System.out.println("  " + task.toString()));
        System.out.println("[OK]");
        return project;
    }

    /**
     * Редактирование атрибутов проекта
     *
     * @param project проект
     * @return проект
     */
    private Project updateProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[EDIT PROJECT]");
        System.out.print("Enter name: ");
        final String name = readString();
        project.setName(name);
        System.out.print("Enter description: ");
        final String description = readString();
        project.setDescription(description);
        System.out.println("[OK]");
        return project;
    }

    /**
     * Удаление проекта из хранилища
     *
     * @param project проект
     * @return проект
     */
    private Project removeProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[REMOVE PROJECT]");
        // отсоединение задач проекта
        List<Task> taskList = projectTaskService.findAllByProjectId(project.getId());
        taskList.forEach(task -> projectTaskService.removeTaskFromProject(project.getId(), task.getId()));
        // удаление проекта из хранилища
        List<Project> projectList = projectService.findAll();
        if (projectList.remove(project))
            System.out.println("[OK]");
        else
            System.out.println("SOMETHING WRONG");
        return project;
    }

    /**
     * Добавление проекта в хранилище
     *
     * @return код возврата
     */
    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        final String name = readString();
        projectService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Вывод списка проектов в хранилище
     *
     * @return код возврата
     */
    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int i = 1;
        for (Project project : projectService.findSortedByName()) {
            System.out.println("[" + (i++) + "]: " + project);
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление всех проектов из хранилища
     *
     * @return код возврата
     */
    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

}
