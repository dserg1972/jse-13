package com.nlmk.dezhemesov.taskmanager.controller;

import com.nlmk.dezhemesov.taskmanager.entity.User;
import com.nlmk.dezhemesov.taskmanager.service.HistoryService;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Родительский класс контроллеров с общей логикой
 */
public abstract class AbstractController {

    /**
     * Обработчик системного ввода
     */
    static final Scanner scanner = new Scanner(System.in);

    /**
     * Чтение команды из системного ввода
     *
     * @return введённая команда
     */
    public String readCommand() {
        String command = readString();
        return command;
    }

    /**
     * Чтение строки из системного ввода
     *
     * @return введённая строка
     */
    public static String readString() {
        String string;
        string = scanner.nextLine();
        return string;
    }

    /**
     * Ввод пароля
     * В эмуляторе консоли IDE пароль не скрыт, в реальной консоли пароль скрыт
     *
     * @return введённый пароль
     */
    public static String readPassword() {
        Console console = System.console();
        String password;
        if (console != null)
            password = String.valueOf(console.readPassword());
        else
            password = readString();
        return password;
    }

    /**
     * Чтение целочисленного значения из системного ввода
     *
     * @return введённая строка, интерпретированная как целочисленное значение
     */
    public static Integer readInteger() {
        Integer value = null;
        try {
            value = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException ex) {
            System.out.println("Can't parse integer value");
        }
        return value;
    }

    /**
     * Чтение Long значения из системного ввода
     *
     * @return введённая строка, интерпретированная как Long значение
     */
    public static Long readLong() {
        Long value = null;
        try {
            value = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ex) {
            System.out.println("Can't parse Long value");
        }
        return value;
    }

}
