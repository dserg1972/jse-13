package com.nlmk.dezhemesov.taskmanager.entity;

/**
 * Сущность "Проект"
 */
public class Project {

    /**
     * Идентификатор экземпляра
     */
    private final Long id = System.nanoTime();

    /**
     * Имя проекта
     */
    private String name = "";

    /**
     * Описание проекта
     */
    private String description = "";

    /**
     * Идентификатор владельца проекта
     */
    private Long userId;

    /**
     * Конструктор по умолчанию
     */
    public Project() {
    }

    /**
     * Конструктор с указанием имени проекта
     *
     * @param name   Имя проекта
     * @param userId идентификатор пользователя
     */
    public Project(final String name, final Long userId) {
        this.name = name;
        this.userId = userId;
    }

    /**
     * Конструктор с заданием имени и описания проекта
     *
     * @param name        имя проекта
     * @param userId      идентификатор создателя проекта
     * @param description описание проекта
     */
    public Project(final String name, final Long userId, final String description) {
        this.name = name;
        this.userId = userId;
        this.description = description;
    }

    /**
     * Получение идентификатора экземпляра
     *
     * @return Идентификатор экземпляра проекта
     */
    public Long getId() {
        return id;
    }

    /**
     * Получение имени проекта
     *
     * @return имя проекта
     */
    public String getName() {
        return name;
    }

    /**
     * Получение описания проекта
     *
     * @return описание проекта
     */
    public String getDescription() {
        return description;
    }

    /**
     * Задание имени проекта
     *
     * @param name новое имя проекта
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Задание описания проекта
     *
     * @param description описание проекта
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Получение идентификатора владельца
     *
     * @return идентификатор владельца
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Установка идентификатора владельца
     *
     * @param userId идентификатор владельца
     */
    public void setUserId(final Long userId) {
        this.userId = userId;
    }

    /**
     * Получение текстового представления данных проекта
     *
     * @return текстовое представление данных проекта
     */
    @Override
    public String toString() {
        return id + ": " + name + " (" + description + ")";
    }
}
