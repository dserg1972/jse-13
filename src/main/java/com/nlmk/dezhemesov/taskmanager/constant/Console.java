package com.nlmk.dezhemesov.taskmanager.constant;

/**
 * Константные значения для использования в консольном модуле
 */
public class Console {

    /**
     * Сведения об авторе
     */
    public static final String ABOUT = "about";
    /**
     * Помощь по ключам запуска
     */
    public static final String HELP = "help";
    /**
     * Сведения о версии
     */
    public static final String VERSION = "version";
    /**
     * Выход из программы
     */
    public static final String EXIT = "exit";


    /**
     * Создание проекта
     */
    public static final String PROJECT_CREATE = "project-create";
    /**
     * Список проектов
     */
    public static final String PROJECT_LIST = "project-list";
    /**
     * Удаление всех проектов
     */

    public static final String PROJECT_CLEAR = "project-erase";
    /**
     * Просмотр проекта по индексу
     */
    public static final String PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    /**
     * Просмотр проекта по имени
     */
    public static final String PROJECT_VIEW_BY_NAME = "project-view-by-name";
    /**
     * Просмотр проекта по идентификатору
     */
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";

    /**
     * Удаление проекта по индексу
     */
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    /**
     * Удаление проекта по имени
     */
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    /**
     * Удаление проекта по идентификатору
     */
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    /**
     * Изменение проекта по индексу
     */
    public static final String PROJECT_EDIT_BY_INDEX = "project-edit-by-index";
    /**
     * Изменение проекта по имени
     */
    public static final String PROJECT_EDIT_BY_NAME = "project-edit-by-name";
    /**
     * Изменение проекта по идентификатору
     */
    public static final String PROJECT_EDIT_BY_ID = "project-edit-by-id";

    /**
     * Создание задачи
     */
    public static final String TASK_CREATE = "task-create";
    /**
     * Список задач
     */
    public static final String TASK_LIST = "task-list";
    /**
     * Удаление всех задач
     */
    public static final String TASK_CLEAR = "task-clear";

    /**
     * Просмотр задачи по индексу
     */
    public static final String TASK_VIEW_BY_INDEX = "task-view-by-index";
    /**
     * Просмотр задачи по имени
     */
    public static final String TASK_VIEW_BY_NAME = "task-view-by-name";
    /**
     * Просмотр задачи по идентификатору
     */
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";

    /**
     * Удаление задачи по индексу
     */
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    /**
     * Удаление задачи по имени
     */
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    /**
     * Удаление задачи по идентификатору
     */
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

    /**
     * Изменение задачи по индексу
     */
    public static final String TASK_EDIT_BY_INDEX = "task-edit-by-index";
    /**
     * Изменение задачи по имени
     */
    public static final String TASK_EDIT_BY_NAME = "task-edit-by-name";
    /**
     * Изменение задачи по идентификатору
     */
    public static final String TASK_EDIT_BY_ID = "task-edit-by-id";

    /**
     * Привязка задачи к проекту
     */
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";

    /**
     * Отсоединение задачи от проекта
     */
    public static final String TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";


    /**
     * Вход/смена пользователя
     */
    public static final String USER_LOGIN = "user-login";
    /**
     * Вывод текущего пользователя
     */
    public static final String USER_CURRENT = "user-current";
    /**
     * Создание нового пользователя
     */
    public static final String USER_CREATE = "user-create";
    /**
     * Вывод списка пользователей
     */
    public static final String USER_LIST = "user-list";
    /**
     * Изменение данных пользователя
     */
    public static final String USER_UPDATE = "user-update";
    /**
     * Удаление пользователя
     */
    public static final String USER_REMOVE = "user-remove";


    public static final String HISTORY_LIST = "history";


}
