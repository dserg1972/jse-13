package com.nlmk.dezhemesov.taskmanager;

import com.nlmk.dezhemesov.taskmanager.controller.*;
import com.nlmk.dezhemesov.taskmanager.enumerated.Role;
import com.nlmk.dezhemesov.taskmanager.exception.DuplicateUserException;
import com.nlmk.dezhemesov.taskmanager.repository.HistoryRepository;
import com.nlmk.dezhemesov.taskmanager.repository.ProjectRepository;
import com.nlmk.dezhemesov.taskmanager.repository.TaskRepository;
import com.nlmk.dezhemesov.taskmanager.repository.UserRepository;
import com.nlmk.dezhemesov.taskmanager.service.*;
import com.nlmk.dezhemesov.taskmanager.util.Identifier;

import java.util.logging.Level;
import java.util.logging.Logger;

import static com.nlmk.dezhemesov.taskmanager.constant.Console.*;

/**
 * Консольный модуль. Точка входа в приложение
 */

public class Application {

    Identifier userId = new Identifier();

    /**
     * Репозиторий проектов
     */
    private final ProjectRepository projectRepository = new ProjectRepository(userId);
    /**
     * Служба проектов
     */
    private final ProjectService projectService = new ProjectService(projectRepository);

    /**
     * Репозиторий задач
     */
    private final TaskRepository taskRepository = new TaskRepository(userId);
    /**
     * Служба задач
     */
    private final TaskService taskService = new TaskService(taskRepository);

    /**
     * Служба с общей бизнес-логикой для проектов и задач
     */
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    /**
     * Контроллер проектов
     */
    private final ProjectController projectController = new ProjectController(projectService, projectTaskService);

    /**
     * Контроллер задач
     */
    private final TaskController taskController = new TaskController(taskService, projectTaskService);


    /**
     * Репозиторий истории команд
     */
    private final HistoryRepository historyRepository = new HistoryRepository();

    /**
     * Сервис истории команд
     */
    private final HistoryService historyService = new HistoryService(historyRepository);

    /**
     * Контроллер системных команд
     */
    private final SystemController systemController = new SystemController(historyService);

    /**
     * Репозиторий пользователей
     */
    private final UserRepository userRepository = new UserRepository();
    /**
     * Служба пользователей
     */
    private final UserService userService = new UserService(userRepository);
    /**
     * Контроллер пользователей
     */
    private final UserController userController = new UserController(userService, historyService, userId);

    {
        try {
            userService.create("admin", "admin", Role.ADMIN);
            userService.create("user1", "user1", Role.ADMIN);
            userService.create("user2", "user2", Role.ADMIN);
        } catch (DuplicateUserException e) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, "Duplicate login!", e);
        }
        //PROJECT1 и его задачи создаются под учётной записью user1
        userId.setIdentifier(userService.findByLogin("user1").getId());
        projectService.create("Z-PROJECT", "Описание проекта Z");
        projectService.create("Y-PROJECT", "Описание проекта Y");
        projectService.create("X-PROJECT", "Описание проекта X");

        projectService.create("PROJECT1", "Описание проекта 1");
        taskService.create("TASK 1", "Описание задачи 1");
        taskService.create("TASK 2", "Описание задачи 2");


        //PROJECT2 и его задачи создаются под учётной записью user2
        userId.setIdentifier(userService.findByLogin("user2").getId());
        projectService.create("PROJECT2", "Описание проекта 2");
        taskService.create("TASK 1", "Описание задачи 1");
        taskService.create("TASK 2", "Описание задачи 2");
        taskService.create("TASK 3", "Описание задачи 3");
        taskService.create("TASK 4", "Описание задачи 4");
    }

    /**
     * Точка входа в приложение
     *
     * @param args - аргументы командной строки
     */
    public static void main(final String[] args) {
        Application application = new Application();
        application.run(args);
        SystemController.displayWelcome();
        application.process();
    }

    /**
     * Основной цикл обработки интерактивных команд
     */
    private void process() {
        int loginResult = userController.login();
        if (loginResult == -1)
            System.exit(-1);
        String command = "";
        while (!EXIT.equals(command)) {
            command = systemController.readCommand();
            run(command);
            System.out.println();
        }
    }

    /**
     * Обработчик ключей запуска
     *
     * @param args аргументы командной строки
     */
    private void run(String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        String command = args[0];
        int result = run(command);
        System.exit(result);
    }

    /**
     * Обработчик команды
     *
     * @param command команда на исполнение
     * @return результат выполнения команды
     */
    private int run(final String command) {
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.exit();

            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_CLEAR:
                return projectController.clearProject();
            case TASK_CREATE:
                return taskController.createTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_CLEAR:
                return taskController.clearTask();

            case PROJECT_VIEW_BY_INDEX:
                return projectController.viewByIndex();
            case PROJECT_VIEW_BY_ID:
                return projectController.viewById();
            case PROJECT_VIEW_BY_NAME:
                return projectController.viewProjectByName();
            case PROJECT_EDIT_BY_INDEX:
                return projectController.updateProjectByIndex();
            case PROJECT_EDIT_BY_ID:
                return projectController.updateProjectById();
            case PROJECT_EDIT_BY_NAME:
                return projectController.updateProjectByName();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();

            case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById();
            case TASK_VIEW_BY_NAME:
                return taskController.viewTaskByName();
            case TASK_EDIT_BY_INDEX:
                return taskController.updateTaskByIndex();
            case TASK_EDIT_BY_ID:
                return taskController.updateTaskById();
            case TASK_EDIT_BY_NAME:
                return taskController.updateTaskByName();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskController.removeTaskFromProjectByIds();

            case USER_LOGIN:
                return userController.login();
            case USER_CURRENT:
                return userController.viewCurrent();
            case USER_CREATE:
                return userController.create();
            case USER_LIST:
                return userController.list();
            case USER_UPDATE:
                return userController.update();
            case USER_REMOVE:
                return userController.remove();

            case HISTORY_LIST:
                return systemController.listHistory();

            default:
                return systemController.displayError();
        }
    }

}
